#!/usr/bin/python

import sys

from mininet.log import setLogLevel, info
from mn_wifi.net import Mininet_wifi
from mn_wifi.cli import CLI
from mn_wifi.link import wmediumd, mesh
from mn_wifi.wmediumdConnector import interference

# script arguments:
# -c : start CLI after experiment (e.g., for manual checks of link/path info)
# -f : start pre-defined flent test
# -m : activate pre-defined mobility
# -p : activate network graph plotting
def myNetwork(args):

    datadir = '/home/wifi/tmp'
    
    net = Mininet_wifi(topo=None,
                       build=False,
                       link=wmediumd,
                       wmediumd_mode=interference,
                       noise_th=-91, fading_cof=0,
                       ipBase='10.0.0.0/24',
                       mode='a') # wmediumd only supports 802.11a/g NOHT rates
    
    info( '*** Adding hosts/stations\n')
    sta1 = net.addStation('sta1', ip='10.0.0.1',
                           prefixLen = '24', 
                           position='200.0,400.0,0',
                           wlans=1)
    sta2 = net.addStation('sta2', ip='10.0.0.2',
                           prefixLen = '24', 
                           position='400.0,400.0,0',
                           wlans=1)
    sta3 = net.addStation('sta3', ip='10.0.0.3',
                           prefixLen = '24', 
                           position='600.0,400.0,0',
                           wlans=1)
    sta4 = net.addStation('sta4', ip='10.0.0.4',
                           prefixLen = '24', 
                           position='800.0,600.0,0',
                           wlans=1)

    info( '*** Configuring propagation model\n')
    net.setPropagationModel(model='logDistance', sL=1, exp=3)

    # use custom mac80211_hwsim module
    #net.setModule('./mac80211_hwsim.ko')

    info( '*** Configuring wifi nodes\n')
    net.configureWifiNodes()
    
    info( '*** Adding mesh interfaces\n')
    net.addLink(sta1, cls=mesh, intf='sta1-wlan0', prefixLen = '24', ssid='meshNet', mode='a', channel=36, ht_cap='NOHT')
    net.addLink(sta2, cls=mesh, intf='sta2-wlan0', prefixLen = '24', ssid='meshNet', mode='a', channel=36, ht_cap='NOHT')
    net.addLink(sta3, cls=mesh, intf='sta3-wlan0', prefixLen = '24', ssid='meshNet', mode='a', channel=36, ht_cap='NOHT')
    net.addLink(sta4, cls=mesh, intf='sta4-wlan0', prefixLen = '24', ssid='meshNet', mode='a', channel=36, ht_cap='NOHT')
    
    # set antenna gain and tx power or directly define range (calculates required tx power)
    #sta1.setAntennaGain(5, intf='sta1-mp0')
    #sta2.setAntennaGain(5, intf='sta2-mp0')
    #sta3.setAntennaGain(5, intf='sta3-mp0')
    #sta4.setAntennaGain(5, intf='sta4-mp0')
    #
    sta1.setTxPower(20, intf='sta1-mp0')
    sta2.setTxPower(20, intf='sta2-mp0')
    sta3.setTxPower(20, intf='sta3-mp0')
    sta4.setTxPower(20, intf='sta4-mp0')
    #
    #sta1.setRange(100, intf='sta1-mp0')
    #sta2.setRange(100, intf='sta2-mp0')
    #sta3.setRange(100, intf='sta3-mp0')
    #sta4.setRange(100, intf='sta4-mp0')
    
    if '-m' in args:
        net.startMobility(time=0, mob_rep=1, reverse=False)
        ps = dict()
        pe = dict()
        ps = {'position': '800.0,600.0,0'}
        pe = {'position': '200.0,600.0,0'}
        net.mobility(sta4, 'start', time=5, **ps)
        net.mobility(sta4, 'stop', time=60, **pe)
        net.stopMobility(time=60)
    
    if '-p' in args:
        net.plotGraph(max_x=1000, max_y=1000)
    
    info( '*** Starting network\n')
    net.build()
    
    info( '*** Configuring mesh interfaces (iw)\n')
    #
    # update regDomain
    sta1.cmd('iw reg set US')
    sta2.cmd('iw reg set US')
    sta3.cmd('iw reg set US')
    sta4.cmd('iw reg set US')
    #
    # set tx power
    #sta1.cmd('iw dev sta1-mp0 set txpower fixed 2000')
    #sta2.cmd('iw dev sta2-mp0 set txpower fixed 2000')
    #sta3.cmd('iw dev sta3-mp0 set txpower fixed 2000')
    #sta4.cmd('iw dev sta4-mp0 set txpower fixed 2000')
    #
    # leave current mesh network before re-joining with own configuration
    sta1.cmd('iw dev sta1-mp0 mesh leave')
    sta2.cmd('iw dev sta2-mp0 mesh leave')
    sta3.cmd('iw dev sta3-mp0 mesh leave')
    sta4.cmd('iw dev sta4-mp0 mesh leave')
    #
    # re-join mesh network on specific channel (with given basic/mcast rates -> 6 (9) 12 18 24 36 48 54)
    #sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 2412 NOHT')
    #sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 2412 NOHT')
    #sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 2412 NOHT')
    #sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 2412 NOHT')
    #
    #sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 2412 NOHT basic-rates 6 mcast-rate 6')
    #sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 2412 NOHT basic-rates 6 mcast-rate 6')
    #sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 2412 NOHT basic-rates 6 mcast-rate 6')
    #sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 2412 NOHT basic-rates 6 mcast-rate 6')
    #
    #sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 2412 NOHT basic-rates 54 mcast-rate 54')
    #sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 2412 NOHT basic-rates 54 mcast-rate 54')
    #sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 2412 NOHT basic-rates 54 mcast-rate 54')
    #sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 2412 NOHT basic-rates 54 mcast-rate 54')
    #
    #sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 5180 NOHT')
    #sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 5180 NOHT')
    #sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 5180 NOHT')
    #sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 5180 NOHT')
    #
    sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 5180 NOHT basic-rates 6 mcast-rate 6')
    sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 5180 NOHT basic-rates 6 mcast-rate 6')
    sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 5180 NOHT basic-rates 6 mcast-rate 6')
    sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 5180 NOHT basic-rates 6 mcast-rate 6')
    #
    #sta1.cmd('iw dev sta1-mp0 mesh join meshNet freq 5180 NOHT basic-rates 54 mcast-rate 54')
    #sta2.cmd('iw dev sta2-mp0 mesh join meshNet freq 5180 NOHT basic-rates 54 mcast-rate 54')
    #sta3.cmd('iw dev sta3-mp0 mesh join meshNet freq 5180 NOHT basic-rates 54 mcast-rate 54')
    #sta4.cmd('iw dev sta4-mp0 mesh join meshNet freq 5180 NOHT basic-rates 54 mcast-rate 54')
    #
    # set tx rates for unicast data frames (no arguments -> RCA)
    # legacy-* -> 802.11 a/g:	6 (9) 12 18 24 36 48 54
    # ht-mcs-* -> 802.11 n:		0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
    # [sgi-2.4|lgi-2.4] [sgi-5|lgi-5] -> short/long guard interval (400/800ns)
    #
    #sta1.cmd('iw dev sta1-mp0 set bitrates')
    #sta2.cmd('iw dev sta2-mp0 set bitrates')
    #sta3.cmd('iw dev sta3-mp0 set bitrates')
    #sta4.cmd('iw dev sta4-mp0 set bitrates')
    #
    #sta1.cmd('iw dev sta1-mp0 set bitrates legacy-2.4 6 lgi-2.4')
    #sta2.cmd('iw dev sta2-mp0 set bitrates legacy-2.4 6 lgi-2.4')
    #sta3.cmd('iw dev sta3-mp0 set bitrates legacy-2.4 6 lgi-2.4')
    #sta4.cmd('iw dev sta4-mp0 set bitrates legacy-2.4 6 lgi-2.4')
    #
    #sta1.cmd('iw dev sta1-mp0 set bitrates legacy-2.4 54 lgi-2.4')
    #sta2.cmd('iw dev sta2-mp0 set bitrates legacy-2.4 54 lgi-2.4')
    #sta3.cmd('iw dev sta3-mp0 set bitrates legacy-2.4 54 lgi-2.4')
    #sta4.cmd('iw dev sta4-mp0 set bitrates legacy-2.4 54 lgi-2.4')
    #
    sta1.cmd('iw dev sta1-mp0 set bitrates legacy-5 6 lgi-5')
    sta2.cmd('iw dev sta2-mp0 set bitrates legacy-5 6 lgi-5')
    sta3.cmd('iw dev sta3-mp0 set bitrates legacy-5 6 lgi-5')
    sta4.cmd('iw dev sta4-mp0 set bitrates legacy-5 6 lgi-5')
    #
    #sta1.cmd('iw dev sta1-mp0 set bitrates legacy-5 54 lgi-5')
    #sta2.cmd('iw dev sta2-mp0 set bitrates legacy-5 54 lgi-5')
    #sta3.cmd('iw dev sta3-mp0 set bitrates legacy-5 54 lgi-5')
    #sta4.cmd('iw dev sta4-mp0 set bitrates legacy-5 54 lgi-5')
    #
    # reduce HWMP path lifetime and update interval on all STAs
    sta1.cmd('iw dev sta1-mp0 set mesh_param mesh_hwmp_active_path_timeout=2000 mesh_path_refresh_time=1000')
    sta2.cmd('iw dev sta2-mp0 set mesh_param mesh_hwmp_active_path_timeout=2000 mesh_path_refresh_time=1000')
    sta3.cmd('iw dev sta3-mp0 set mesh_param mesh_hwmp_active_path_timeout=2000 mesh_path_refresh_time=1000')
    sta4.cmd('iw dev sta4-mp0 set mesh_param mesh_hwmp_active_path_timeout=2000 mesh_path_refresh_time=1000')
    
    info( '*** Starting netperf and iperf servers on sta1\n')
    sta1.cmd('netserver &')
    sta1.cmd('iperf -s &')
    
    if '-f' in args:
        info( '*** Starting flent on sta4\n')
        #
        #sta4.cmd('flent mesh_tcp_up_noPing -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))		# TCP-UL (without Ping)
        #sta4.cmd('flent mesh_tcp_up -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))			# TCP-UL, Ping ICMP
        sta4.cmd('flent mesh_tcp_down -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))											# TCP-DL, Ping ICMP
        #sta4.cmd('flent mesh_tcp_up_down -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))		# TCP-UL, TCP-DL, Ping ICMP
        #
        #sta4.cmd('flent mesh_tcp_up_BE -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))			# TCP-UL, Ping ICMP (QoS = BE)
        #sta4.cmd('flent mesh_tcp_up_VO -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))			# TCP-UL, Ping ICMP (QoS = VO)
        #sta4.cmd('flent mesh_tcp_down_BE -l 60 -d 0 --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))						# TCP-DL, Ping ICMP (QoS = BE)
        #sta4.cmd('flent mesh_tcp_down_VO -l 60 -d 0 --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))						# TCP-DL, Ping ICMP (QoS = VO)
        #
        #sta4.cmd('flent mesh_tcp_up_VO_BE -l 60 -d 0 --socket-stats --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))		# TCP-UL, Ping ICMP (QoS = BE) + TCP-UL, Ping ICMP (QoS = VO)
        #sta4.cmd('flent mesh_tcp_down_VO_BE -l 60 -d 0 --send-size 8192 -H 10.0.0.1 -D {0}'.format(datadir))					# TCP-DL, Ping ICMP (QoS = BE) + TCP-DL, Ping ICMP (QoS = VO)
        #
        #sta4.cmd('flent mesh_ping_VO_BE -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))										# Ping ICMP (QoS = BE), Ping ICMP (QoS = VO)
        #
        #sta4.cmd('flent --test-parameter udp_bandwidth=6M --test-parameter udp_pktsize=1472 mesh_udp_up -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))		# UDP-UL, Ping ICMP
        #sta4.cmd('flent --test-parameter udp_bandwidth=6M --test-parameter udp_pktsize=1472 mesh_udp_up_BE -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))		# UDP-UL, Ping ICMP (QoS = BE)
        #sta4.cmd('flent --test-parameter udp_bandwidth=6M --test-parameter udp_pktsize=1472 mesh_udp_up_VO -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))		# UDP-UL, Ping ICMP (QoS = VO)
        #sta4.cmd('flent --test-parameter udp_bandwidth=6M --test-parameter udp_pktsize=1472 mesh_udp_up_VO_BE -l 60 -d 0 -H 10.0.0.1 -D {0}'.format(datadir))	# UDP-UL (QoS = BE), UDP-UL (QoS = VO)
    
    if '-c' in args:
        info( '*** Starting CLI\n')
        CLI(net)
    
    info( '*** Killing netperf and iperf servers on sta1\n')
    sta1.cmd('pkill netserver')
    sta1.cmd('pkill iperf')
    
    net.stop()
    
    
if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork(sys.argv)
