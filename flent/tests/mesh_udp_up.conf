## -*- mode: python; coding: utf-8 -*-

include("netperf_definitions.inc")
DESCRIPTION="Single UDP Upload"
DEFAULTS={'PLOT': 'totals'}

BW=get_test_parameter('udp_bandwidth')
PKTSIZE=get_test_parameter('udp_pktsize')
MARKING=try_test_parameters(('marking', 'markings'), default=["CS0,CS0"], split=True)[0]

DATA_SETS = o([
        ('UDP upload',
         {'host': HOST, 'interval': STEP_SIZE, 'length': LENGTH, 'udp': 'True', 'bw': BW, 'pktsize': PKTSIZE, 'delay': DELAY, 
          'marking': MARKING, 'ip_version' : IP_VERSION, 'units': 'Throughput [Mbits/s]', 'runner': 'iperf_csv'}),
        ('Ping (ms)',
         {'ip_version': IP_VERSION, 'interval': STEP_SIZE, 'length': TOTAL_LENGTH,
          'marking': MARKING, 'host': HOST, 'units': 'Ping RTT [ms]', 'runner': 'ping'}),
        ('Mesh',
         {'length': LENGTH, 'interval': STEP_SIZE, 'units': '', 'runner': 'mesh'}),
        ])


PLOTS = o([
    ('totals',
     {'description': '   ',
      'type': 'timeseries',
      'dual_axes': True,
      'series': [{'data': 'UDP upload',
                  'label': 'Upload'},
                 {'data': 'Ping (ms)',
                  'label': 'Ping',
                  'axis': 2}]}),

    ('Mesh_Info1',
     {'description': 'Signal Strength',
      'type': 'timeseries',
      'axis_labels': ['RSS [dBm]'],
      'series': [{'data': glob('RSS**'),
                  'label': ''
                  }
                 ]}),
    ('Mesh_Info2',
     {'description': 'Metric ALM',
      'type': 'timeseries',
      'axis_labels': ['ALM [10µs]'],
      'series': [
                 {'data': glob('ALM**'),
                  'label': ''
                  },
                 ]}),
    ('Mesh_Info3',
     {'description': 'Hop Count',
      'type': 'timeseries',
      'axis_labels': ['Hops'],
      'series': [
                 {'data': glob('Hops**'),
                  'label': ''
                  },
                 ]}),
    ('box_totals',
     {'description': 'Box plot of totals',
      'parent': 'totals',
      'type': 'box',
      'cutoff': (DELAY,-DELAY)}),
    ('bar_totals',
     {'description': 'Bar plot of totals',
      'parent': 'totals',
      'type': 'bar',
      'cutoff': (DELAY,-DELAY)}),
    ])

include("common.inc")
