#!/usr/bin/python3

import hashlib
import hmac
import math
import os
import shlex
import signal
import socket
import tempfile
import threading
import time
import re
import subprocess
import sys
from datetime import datetime

# args: devices, length, interval
devices = str(sys.argv[1])[1:-1].split()
length = int(sys.argv[2])
interval = float(sys.argv[3])

loops = int(int(length)/float(interval))

timestamps = []
station_results = {}
mpath_results = {}

timestamps = [0.0] * loops

for dev in devices:
    mpath_results[dev] = []
    station_results[dev] = []
    
for loop in range(loops):
    n = -1
    current_time = datetime.now().timestamp()
    timestamps[loop] = current_time
    
    for dev in devices:
        n += 1
        
        # run iw commands
        comm1 = "iw dev {device} station dump".format(device=dev)
        comm2 = "iw dev {device} mpath dump | tr -s ' '".format(device=dev)
        outp1 = subprocess.check_output(comm1, shell=1).decode("utf-8")
        outp2 = subprocess.check_output(comm2, shell=1).decode("utf-8")

        # process station dump
        outp1 = ["Station "+e for e in outp1.split("Station ") if e]
        stations = []
        signals = []
        for i in range(len(outp1)):
            stations.append(re.search('Station (.*) \(', outp1[i]).group(1))
            signals.append(re.search('signal:  \t(.+?)dBm', outp1[i]).group(1))
        station_dict = dict()
        station_dict["stations"] = stations
        station_dict["signals"] = signals
        station_results[dev].append(station_dict)

        # process mpath dump
        lines = re.split('\n', outp2)
        del lines[0]
        del lines[len(lines) - 1]
        n = 0
        table = []
        for line in lines:
            pairs = re.split(' |\n|\t', line)
            bib = dict()
            bib["Destination"] = pairs[0]
            bib["Next Hop"] = pairs[1]
            bib["Metric"] = pairs[4]
            bib["Hops"] = pairs[10]
            table.append(bib)
            n += 1
        if table:
            pass
        else:
            print("no paths found")
        mpath_results[dev].append(table)

    time.sleep(interval)

print(timestamps)
print(station_results)
print(mpath_results)
